from django.db import models
from django.contrib.auth.models import User

from core.models import BaseModel


class Todo(BaseModel):
    body = models.TextField(max_length=256)
    done = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'Body: {self.body},\nDone: {self.done},\nCreated by: {self.created_by}'

    class Meta:
        ordering = ['done']
