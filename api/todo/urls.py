from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('all/', views.get_todos, name='get_todos'),
    path('get/<str:uid>', views.get_todo_by_id, name='get_todo_by_id'),
    path('add/', views.add_todo, name='add_todo'),
    path('update/<str:uid>', views.update_todo, name='update_todo'),
    path('delete/<str:uid>', views.delete_todo, name='delete_todo'),
]
