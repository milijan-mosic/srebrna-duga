from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from rest_framework import exceptions
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .serializers import *
from .models import *


@login_required
def index(request):
    return HttpResponse('Hello from the Todo app!')


@api_view(['GET'])
def get_todos(request):
    try:
        queryset = Todo.objects.all().filter(created_by=request.user.id).order_by('uid')
        todos = TodoSerializer(queryset, many=True)

        return Response(todos.data)

    except Exception as e:
        print(e)
        message: dict = {'message': 'Getting todos -> FAILED'}
        print(message)

        return Response(message)


@api_view(['GET'])
def get_todo_by_id(request, uid):
    try:
        queryset = Todo.objects.get(uid=uid, created_by=request.user.id)
        todo = TodoSerializer(queryset, many=False)

        return Response(todo.data)

    except Todo.DoesNotExist as e:
        exc = exceptions.NotFound()
        message: dict = {'message': 'Todo ' + exc.detail}
        print(message)

        return Response(message, exc.status_code)


@api_view(['POST'])
def add_todo(request):
    try:
        data_for_db: dict = request.data
        data_for_db['created_by'] = request.user.id
        new_todo = TodoSerializer(data=data_for_db)

        if new_todo.is_valid():
            new_todo.save()

            return Response(new_todo.data)
        else:
            return Response('Data is not valid!')

    except Exception as e:
        print(e)
        message: dict = {'message': 'Adding -> FAILED'}
        print(message)

        return Response(message)


@api_view(['PUT'])
def update_todo(request, uid):
    try:
        todo = Todo.objects.get(uid=uid, created_by=request.user.id)
        updated_todo = UpdateTodoSerializer(instance=todo, data=request.data)

        if updated_todo.is_valid():
            updated_todo.save()

            return Response(updated_todo.data)
        else:
            return Response('Data is not valid!')

    except Exception as e:
        print(e)
        message: dict = {'message': 'Updating -> FAILED'}
        print(message)

        return Response(message)


@api_view(['DELETE'])
def delete_todo(request, uid):
    try:
        todo = Todo.objects.get(uid=uid, created_by=request.user.id)
        todo.delete()

        return Response({'message': 'Deleting todo was successfull'})

    except Exception as e:
        print(e)
        message: dict = {'message': 'Deleting -> FAILED'}
        print(message)

        return Response(message)
