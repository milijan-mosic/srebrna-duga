FROM python:3.11-alpine

ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /code
COPY . /code/.
WORKDIR /code

EXPOSE 5005

RUN pip install -r requirements.txt
CMD python manage.py runserver 0.0.0.0:5005
