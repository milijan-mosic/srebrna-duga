from django.contrib.auth.models import User

from rest_framework import routers, serializers, viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from .serializers import *


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)


@api_view(['GET'])
def get_all_users(request):
    all_users = User.objects.all()
    serialized_data = UserSerializer(all_users, many=True)

    return Response(serialized_data.data)


@api_view(['POST'])
@permission_classes([AllowAny])
def register_user(request):
    try:
        data_for_db: dict = request.data
        queryset = User.objects.get(email=data_for_db['email'])
        if queryset:
            return Response('User with this email already exists')

        new_user = RegisterUserSerializer(data=request.data)

        if new_user.is_valid():
            new_user.save()

            return Response(new_user.data)
        else:
            return Response('Data is not valid!')

    except Exception as e:
        print(e)
        message: dict = {'message': 'Adding -> FAILED'}
        print(message)

        return Response(message)
