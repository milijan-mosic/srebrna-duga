from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenBlacklistView
from rest_framework.schemas import get_schema_view

from .views import router
from . import views


URL_PREFIX: str = 'api/'


urlpatterns = [
    # ADMIN ROUTES
    path(f'{URL_PREFIX}staff/', include(router.urls)),
    path(f'{URL_PREFIX}admin/', admin.site.urls),
    # path(f'{URL_PREFIX}staff/', views.user_list, name='user_list'),

    # AUTH
    path(f'{URL_PREFIX}api-auth/', include('rest_framework.urls')),
    path(f'{URL_PREFIX}auth/register/', views.register_user, name='register_user'),
    path(f'{URL_PREFIX}auth/login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path(f'{URL_PREFIX}auth/logout/', TokenBlacklistView.as_view(), name='token_blacklist'),
    path(f'{URL_PREFIX}auth/token-refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    # DJ
    path(f'{URL_PREFIX}login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path(f'{URL_PREFIX}logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),

    # SWAGGER UI
    path(f'{URL_PREFIX}openapi/', get_schema_view(
        title='Muh Docs!',
        description='API documentation for whole project',
        version='1.0.0'
    ), name='openapi-schema'),

    path(f'{URL_PREFIX}swagger-ui/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),

    # APPS
    path(f'{URL_PREFIX}todo/', include('todo.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
