# Vibe

## Development setup

### 1. Open `hosts` file in the /etc/ dir.

```sh
sudo nvim /etc/hosts
```

### 2. Copy and paste these lines of code.

```sh
################################################

127.0.0.1    app.vibe.local.com
127.0.0.1    pgadmin.vibe.local.com

################################################
```

### 3. Run Docker Compose

```sh
docker-compose up --build --remove-orphans
```
