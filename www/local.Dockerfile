FROM node:20-alpine

RUN mkdir -p /code
COPY . /code/.
WORKDIR /code

EXPOSE 3000

RUN npm ci
CMD npm run dev
